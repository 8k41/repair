﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Musics
{
    public class Etc : AbstractMusic
    {
        private string _typevoice;

        public string TypeVoice
        {
            get { return _typevoice; }
            set { _typevoice = value; }
        }
    }
}
