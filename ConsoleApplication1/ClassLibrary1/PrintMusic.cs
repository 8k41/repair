﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Musics;

namespace PrintMusic
{
    public class Printmusic
    {
        public void print(Disk mydisk, Calculation calc)
        {
            int totalcal = calc.GetLength(mydisk);
            foreach (AbstractMusic music in mydisk.Music)
            {
                Console.WriteLine(music.Author + " " + music.Jenre + " " + music.Length + "s");
            }
            Console.WriteLine("Total Time = " + totalcal + "s");
            Console.ReadKey();
        }
    }
}
